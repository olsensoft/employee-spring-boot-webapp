FROM openjdk:11.0

ADD target/employee-spring-boot-webapp-0.0.1.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/app.jar"]
